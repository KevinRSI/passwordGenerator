//on va récupérer les inputs et l'endroit où s'affichera les mot de pass avec des querySelector
let numberOfChar = document.querySelector('#howmany')
let passwordResult = document.querySelector('#show')
let checkNumbers = document.querySelector('#numbers')
let checkUpper = document.querySelector('#uppercase')
let checkSpecials = document.querySelector('#specials')
// on déclare tous les caractères qu'on utilisera potentiellement dans la création du mot de passe
const characters = "abcdefghijklmnopqrstuvwxyz"
const numbers = '0123456789'
const specials = '!@#$%^&*()'
const uppercaseChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
// on déclare un tableau afin de pouvoir spécifier plus tard quels éléments on utilisera dans la création
let types = [characters]
//ici on dit au code qu'il doit déclencher une action au submit du formulaire
document.querySelector('#formGenerator').addEventListener('submit', (event)=>{
    // on utilise des if/else en version ternaire pour ajouter ou supprimer les différents élements en fonction de si l'élément html correspondant à été check
    checkNumbers.checked ? types.push(numbers) : types = types.filter(item => item !== numbers)
    checkUpper.checked ? types.push(uppercaseChar) : types = types.filter(item => item !== uppercaseChar)
    checkSpecials.checked ? types.push(specials) : types = types.filter(item => item !== specials)
    //ensuite on lance la fonction qui générera le mot de passe avec en paramètre la valeur qu'on a entré dans l'input
    generatePassword(Number(numberOfChar.value));
    //et là on empêche au formulaire de reload la page quand il submit
    event.preventDefault();
})



function generatePassword(number){
    //on déclare un mot de passe vide
    let password = '';
    //ici on utilise une boucle qui va partir de 0 et s'arrêter au nombre qu'on a écrit dans l'input
    for (let i = 0; i < number; i++) {
        //ici on créé des nombres aléatoires, un pour l'index du tableau types afin de choisir aléatoirement un type de caractère
        //et un pour choisir aléatoirement le caractère à l'intérieur du type sélectionné
        let randomType = Math.floor(Math.random() * types.length);
        let randomNumber = Math.floor(Math.random() * types[randomType].length);
        //ici on ajoute à password le caractère trouvé via les nombres aléatoires en utilisant substring
        password += types[randomType].substring(randomNumber, randomNumber + 1)
    }
    // enfin on ajoute le mot de passe généré à l'élément html
    passwordResult.innerHTML = password
}

